#!/bin/bash

set -e

usage="
$(basename "$0") [-h]

$(basename "$0") analyze analyzer-commands

where:
  -h  show this help text"

if [[ $# -eq 0 ]] ; then
  echo "$usage"
  exit 1
fi

while getopts ':h' option; do
  case "$option" in
    h) echo "$usage"
       exit
       ;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2
       echo "$usage" >&2
       exit 1
       ;;
  esac
done
shift $((OPTIND - 1))

COMMAND=$1
shift

case "$COMMAND" in
  "analyse"|"analyze")
    source ${SDKMAN_DIR}/bin/sdkman-init.sh

    case "$SAST_JAVA_VERSION" in
        "8")  sdk default java ${JAVA_8_VERSION}
              ;;
        "11") sdk default java ${JAVA_11_VERSION}
              ;;
        "")   sdk default java ${JAVA_8_VERSION}
              ;;
        *)    echo "Java version $SAST_JAVA_VERSION is not supported. Supported values are 8, 11."
              exit 1
              ;;
    esac

    ANALYZER_COMMAND=$1

    # default to run if no command is given
    if [[ "$ANALYZER_COMMAND" == "" ]] ; then
      ANALYZER_COMMAND="run"
    else
      ANALYZER_COMMAND=$@
    fi

    /analyzer ${ANALYZER_COMMAND}
    ;;
  *)
    echo "$usage"
    exit 1
    ;;
esac
