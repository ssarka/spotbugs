package sdkman

import (
  "fmt"
  "os/exec"
  "path/filepath"

  "github.com/logrusorgru/aurora"
  "github.com/urfave/cli"
)

const (
  FlagJavaPath      = "javaPath"
  FlagJavaVersion   = "javaVersion"
  FlagJava8Version  = "java8Version"
  FlagJava11Version = "java11Version"
  FlagSdkmanDir     = "sdkmanDir"
)

// set system java so that SpotBugs and it's dependencies (e.g. Maven) use the same Java
func SetupSystemJava(c *cli.Context) {
	if usesCustomJavaPath(c) {
		return
	}

	sdkmanDir := c.String(FlagSdkmanDir)
	javaVersion := selectedSystemJava(c)

	cmd := exec.Command(
					"/bin/bash",
					"-c",
					fmt.Sprintf("source %[1]s/bin/sdkman-init.sh" +
											// can exit 1 if already installed
											" && (sdk list java | grep -qv \"installed.*%[2]s\" || sdk install java %[2]s)" +
										  " && sdk default java %[2]s", sdkmanDir, javaVersion))
	output, err := cmd.CombinedOutput()

	_, _ = fmt.Fprintf(c.App.Writer, "%s", output)

	if err != nil {
		_, _ = fmt.Fprintln(c.App.Writer, aurora.Sprintf(
			aurora.Brown("Warning: Failed to set system Java: %s"),
			err.Error()))

		return
	}
}

// determine the path to the java executable
func JavaPath(c *cli.Context) string {
  if usesCustomJavaPath(c) {
    return c.String(FlagJavaPath)
  }

	return filepath.FromSlash(fmt.Sprintf("%s/candidates/java/current/bin/java", c.String(FlagSdkmanDir)))
}

// returns based on whether or not using a custom Java
func usesCustomJavaPath(c *cli.Context) bool {
  return c.String(FlagJavaPath) != "java" && c.String(FlagJavaPath) != ""
}

// determine the version of the selected Java to use
func selectedSystemJava(c *cli.Context) string {
	switch c.String(FlagJavaVersion) {
	case "11":
		return c.String(FlagJava11Version)
	case "8":
		return c.String(FlagJava8Version)
	default:
		_, _ = fmt.Fprintln(c.App.Writer, aurora.Sprintf(
			aurora.Brown("Warning: Java version %s is not supported. Valid values are 8, 11. Using Java 8."),
			c.String(FlagJavaVersion)))
		return c.String(FlagJava8Version)
	}
}
