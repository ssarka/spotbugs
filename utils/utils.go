package utils

import (
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"syscall"

	"github.com/urfave/cli"

	"github.com/logrusorgru/aurora"
)

type RunCmdError struct {
	err      string
	exitCode int
}

func NewRunCmdError(exitCode int, message string) error {
	return &RunCmdError{
		err:      message,
		exitCode: exitCode,
	}
}

func (e *RunCmdError) Error() string {
	return e.err
}

// RunCmd runs a command and gets the exit code.
func RunCmd(cmd *exec.Cmd) error {
	err := cmd.Run()

	if err != nil {
		switch err.(type) {
		case *exec.ExitError:
			// The command failed during its execution
			exitError := err.(*exec.ExitError)
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			return NewRunCmdError(waitStatus.ExitStatus(), err.Error())
		default:
			// The command couldn't even be executed
			return NewRunCmdError(1, fmt.Sprintf("Command couldn't be executed: %v", err))
		}
	}

	waitStatus := cmd.ProcessState.Sys().(syscall.WaitStatus)
	exitStatus := waitStatus.ExitStatus()

	if exitStatus != 0 {
		return NewRunCmdError(exitStatus, "Command returned a non zero exit status")
	}

	return nil
}

// RunCmdWithTextErrorDetection runs a command and returns an error code according to the presence
// of a string in the output.
// Uses code from https://github.com/kjk/go-cookbook in the public domain
func RunCmdWithTextErrorDetection(cmd *exec.Cmd, c *cli.Context, errorText string, message string) error {
	output, err := cmd.CombinedOutput()
	if err != nil {
		return err
	}

	// Print output
	_, _ = fmt.Fprintln(c.App.Writer, string(output))

	// Detect error string
	if strings.Index(string(output), errorText) != -1 {
		// Error text is present, return error.
		return errors.New(message)
	}

	return nil
}

// SetupCmd sets up a command's directory, environment and standard out and err for execution.
func SetupCmd(projectPath string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return SetupCmdNoStd(projectPath, cmd)
}

// SetupCmdNoStd sets up a command's directory and environment for execution.
func SetupCmdNoStd(projectPath string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = projectPath
	cmd.Env = os.Environ()
	return cmd
}

// WithWarning runs the function passed as argument and prints a warning if it returns an error
func WithWarning(writer io.Writer, warning string, fun func() error) {
	err := fun()
	if err != nil {
		_, _ = fmt.Fprintln(writer, aurora.Sprintf(
			aurora.Brown("Warning: %s (%s)\n"),
			warning,
			err.Error()))
	}
}
