// Package bug_instance contains the struct type mapping to SpotBugs XML output.
// issue fields.
package bug_instance

import (
	"fmt"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// BugInstances maps to SpotBugs reports' root XML element.
type BugInstances struct {
	BugInstances []BugInstance `xml:"BugInstance"`
}

// BugInstance maps to a bug - in our case a vulnerability - in the SpotBugs report.
type BugInstance struct {
	Type         string `xml:"type,attr"`
	CWEID        int    `xml:"cweid,attr"`
	Rank         int    `xml:"rank,attr"`
	Priority     int    `xml:"priority,attr"`
	InstanceHash string `xml:"instanceHash,attr"`
	ShortMessage string `xml:"ShortMessage"`
	LongMessage  string `xml:"LongMessage"`
	Class        struct {
		Name string `xml:"classname,attr"`
	} `xml:"Class"`
	Method struct {
		Name string `xml:"name,attr"`
	} `xml:"Method"`
	SourceLine SourceLine `xml:"SourceLine"` // explicit SourceLine type annotation required to make XML marshaling work
}

// SourceLine maps to a location of a vulnerability (source code file, start line, end line) in the SpotBugs report.
type SourceLine struct {
	Start      int    `xml:"start,attr"`
	End        int    `xml:"end,attr"`
	SourcePath string `xml:"sourcepath,attr"`
}

// CompareKey returns a string used to establish whether two issues are the same.
func (bug BugInstance) CompareKey() string {
	fields := []string{
		bug.InstanceHash,
		bug.Type,
		bug.SourceLine.SourcePath,
		strconv.Itoa(bug.SourceLine.Start),
	}

	key := strings.Join(fields, ":")

	if key == ":::0" {
		return ""
	}

	return key
}

// Severity returns the normalized Severity of the issue.
// See https://github.com/spotbugs/spotbugs/blob/3.1.1/spotbugs/src/main/java/edu/umd/cs/findbugs/BugRankCategory.java#L32
func (bug BugInstance) Severity() issue.SeverityLevel {
	switch bug.Rank {
	case 1, 2, 3, 4:
		return issue.SeverityLevelCritical
	case 5, 6, 7, 8, 9:
		return issue.SeverityLevelHigh
	case 10, 11, 12, 13, 14:
		return issue.SeverityLevelMedium
	case 15, 16, 17, 18, 19, 20:
		return issue.SeverityLevelLow
	}
	return issue.SeverityLevelUnknown
}

// Confidence returns the normalized Confidence of the issue.
// See https://github.com/spotbugs/spotbugs/blob/3.1.1/spotbugs/src/main/java/edu/umd/cs/findbugs/Priorities.java
func (bug BugInstance) Confidence() issue.ConfidenceLevel {
	switch bug.Priority {
	case 1:
		return issue.ConfidenceLevelHigh
	case 2:
		return issue.ConfidenceLevelMedium
	case 3:
		return issue.ConfidenceLevelLow
	case 4:
		return issue.ConfidenceLevelExperimental
	case 5:
		return issue.ConfidenceLevelIgnore
	}
	return issue.ConfidenceLevelUnknown
}

// Location returns a structured Location.
func (bug BugInstance) Location(prependPath string) issue.Location {
	return issue.Location{
		File:      filepath.Join(prependPath, bug.SourceLine.SourcePath),
		LineStart: bug.SourceLine.Start,
		LineEnd:   bug.SourceLine.End,
		Class:     bug.Class.Name,
		Method:    bug.Method.Name,
	}
}

// Identifiers returns the normalized Identifiers of the issue.
func (bug BugInstance) Identifiers() []issue.Identifier {
	identifiers := []issue.Identifier{
		bug.fSBIdentifier(),
	}

	// Add CWE ID
	if bug.CWEID != 0 {
		identifiers = append(identifiers, issue.CWEIdentifier(bug.CWEID))
	}

	return identifiers
}

// fSBIdentifier returns a structured Identifier for a FSB bug Type
func (bug BugInstance) fSBIdentifier() issue.Identifier {
	return issue.Identifier{
		Type:  "find_sec_bugs_type",
		Name:  fmt.Sprintf("Find Security Bugs-%s", bug.Type),
		Value: bug.Type,
		URL:   fmt.Sprintf("https://find-sec-bugs.github.io/bugs.htm#%s", bug.Type),
	}
}

// Sorting
type By func(b1, b2 *BugInstance) bool

func (by By) Sort(bugInstances []BugInstance) {
	bs := &bugInstanceSorter{
		bugInstances: bugInstances,
		by:           by,
	}
	sort.Sort(bs)
}

type bugInstanceSorter struct {
	bugInstances []BugInstance
	by           func(b1, b2 *BugInstance) bool
}

func (b *bugInstanceSorter) Len() int {
	return len(b.bugInstances)
}

func (b *bugInstanceSorter) Swap(i, j int) {
	b.bugInstances[i], b.bugInstances[j] = b.bugInstances[j], b.bugInstances[i]
}

func (b *bugInstanceSorter) Less(i, j int) bool {
	return b.by(&b.bugInstances[i], &b.bugInstances[j])
}
